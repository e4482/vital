<?php
// This file is part of The Bootstrap 3 Moodle theme
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = false;

$knownregionpre = $PAGE->blocks->is_known_region('side-pre');
$knownregionpost = false;

$regions = vital_grid($hassidepre, $hassidepost);
$PAGE->set_popup_notification_allowed(false);
$PAGE->requires->jquery();
$PAGE->requires->js_call_amd('theme_bootstrap/optin', 'init');

// L'icône Éléa redirige l'utilisateur selon son profil
// La redirection automatique à la connexion est assurée par le patch "redirectspecificusers" !
if (get_capability_info('local/platformagent:school') && has_capability('local/platformagent:school', context_system::instance())) {
    $urlway = '/local/platformagent/index.php?way=school.page.index';
} else if (get_capability_info('local/trainingtroops:sandbox') && has_capability('local/trainingtroops:sandbox', context_system::instance()) && get_config('local_trainingtroops', 'bac_on')) {
    $urlway = '/local/trainingtroops/index.php?way=sandbox.page.index';
} else if (get_capability_info('local/massimilate:import') && has_capability('local/massimilate:import', context_system::instance())) {
    $urlway = '/local/massimilate/index.php?way=import.page.index';
} else {
    $urlway = '/local/teacherboard/';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
  <title><?php echo theme_vital_get_hostname(); ?></title>
  <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
  <?php echo $OUTPUT->standard_head_html(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
</head>

<body <?php echo $OUTPUT->body_attributes(['vital']); ?>>
<?php echo $OUTPUT->standard_top_of_body_html() ?>
<button id="showRight"></button>
<nav role="navigation" class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" style='padding-top:0px;' href="<?php echo $CFG->wwwroot.$urlway ?>">
        <img src='<?php echo $OUTPUT->image_url('logo', 'theme'); ?>' alt='ELEA' style='height:62px;' />
      </a>
    </div>
  </div>
</nav>

<div id="page" class="container-fluid">
    <header id="page-header" class="clearfix">
        <div id="page-navbar" class="clearfix">
            <nav class="breadcrumb-nav" role="navigation" aria-label="breadcrumb">
              <?php echo $OUTPUT->navbar(); ?>
            </nav>
            <div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
            <?php if ($knownregionpre || $knownregionpost) { ?>
                <div class="breadcrumb-button"> <?php echo $OUTPUT->content_zoom(); ?></div>
            <?php } ?>
        </div>

        <div id="course-header">
            <?php echo $OUTPUT->course_header(); ?>
        </div>
    </header>

<?php
  global $PAGE, $COURSE, $USER;

  $it_is_a_module = preg_match("/(.*)\/mod\/(.*)\/view.php\?id=([0-9]*)/",
    $PAGE->url,
    $results);
  if ($it_is_a_module) {
    $module_type = $results[2];
    $module_id = $results[3];
  }
  else {
    $it_is_a_module = preg_match("/(.*)\/mod\/data\/(.*)\?d=([0-9]*)/",
      $PAGE->url,
      $results);
    if ($it_is_a_module) {
      $module_type = "data";
      $cm = get_coursemodule_from_instance('data', $results[3], $COURSE->id);
      $module_id = $cm->id;
    } else {
        $it_is_a_module = preg_match("/(.*)\/mod\/forum\/(.*)\?f=([0-9]*)/",
            $PAGE->url,
            $results);
        if ($it_is_a_module) {
            $module_type = "forum";
            $cm = get_coursemodule_from_instance('forum', $results[3], $COURSE->id);
            $module_id = $cm->id;
        }
    }
  }

?>


    <div id="page-content" class="row">
        <div id="region-main" class="<?php echo $regions['content']; ?>">

      <?php
        if ($it_is_a_module) {
          $context = context_module::instance($module_id);
          $course_context = context_course::instance($COURSE->id);
          $canmanageactivities = has_capability('moodle/course:manageactivities', $course_context, $USER->id);
          if ($canmanageactivities) {
            if ($module_type == "quiz") {
              echo "<a href='/course/modedit.php?update=".$module_id."&return=1'><img style='margin-left:5px;width:70px;float:right;position:relative;z-index:100;' src='/theme/vital/pix/options.png' alt='options' title='paramètres'/></a>";
              echo "<a href='/mod/quiz/edit.php?cmid=".$module_id."'><img style='width:70px;float:right;position:relative;z-index:100;' src='/theme/vital/pix/edit.png' alt='edit' title='gérer les questions'/></a>";
            }
            else if ($module_type == "simpleqcm") {
              echo "<a href='/mod/simpleqcm/grade.php?id=".$module_id."'><img style='margin-left:5px;width:70px;float:right;position:relative;z-index:100;' src='/theme/vital/pix/stats.png' alt='stats' title='voir les statistiques'/></a>";
              echo "<a href='/course/modedit.php?update=".$module_id."&return=1'><img style='width:70px;float:right;position:relative;z-index:100;' src='/theme/vital/pix/edit.png' alt='edit' title='éditer'/></a>";
            }
            else {
              echo "<a href='/course/modedit.php?update=".$module_id."&return=1'><img style='width:70px;float:right;position:relative;z-index:100;' src='/theme/vital/pix/edit.png' alt='edit' title='éditer'/></a>";
            }
          }
        }
      ?>
          <?php
          echo $OUTPUT->course_content_header();
          echo $OUTPUT->main_content();
          echo $OUTPUT->course_content_footer();
          ?>
        </div>

<?php
  /*****************************************************
   *
   * Hack to detect if we are in a view module page
   * In this case, display a nav button to move forward
   * to the next module
   *
   *****************************************************/

  if ($it_is_a_module) {
    $navigationModuleButton = new navigationModule($COURSE->id, $module_type, $module_id);
    list($previousModule, $nextModule) = $navigationModuleButton->html();
    list($allow_nextModule, $allow_previousModule) = $navigationModuleButton->permissions($PAGE->url);
    if ($allow_nextModule) echo $nextModule;
    if ($allow_previousModule) echo $previousModule;

  }
?>


        <div id="menubody" style='overflow:auto;'>
            <div id="menulevel2">
                <nav class="cbp-spmenu" id="cbp-spmenu-s2" style="overflow-x: visible;">
                    <div id="moodle-navbar">
                        <?php echo $OUTPUT->custom_menu(); ?>
                        <?php echo $OUTPUT->user_menu(); ?>
                        <ul class="nav pull-right">
                            <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                        </ul>
                    </div>
                    <?php
                    if ($knownregionpre) {
                        echo $OUTPUT->blocks('side-pre', $regions['pre']);
                    }?>
                    <?php
                    if ($knownregionpost) {
                        echo $OUTPUT->blocks('side-post', $regions['post']);
                    }?>
                </nav>
                <p style="position:absolute;top:10px;left:10px;">
                  <img id='close_menu2' style='cursor:pointer;height:40px' src='<?php echo $OUTPUT->pix_url('return', 'theme'); ?>'>
                </p>
            </div>
            <p style="position:absolute;top:10px;left:10px;">
              <img id='close_menu1' style='cursor:pointer;height:40px' src='<?php echo $OUTPUT->pix_url('return', 'theme'); ?>'>
            </p>
        </div>

    </div>


    <?php
      /*
       *
       *  DISPLAY BADGE IF AWARDED
       *  REFACTORING NEEDED
       *  Event used to catch badge awarding
       *  see classes/group_observers.php
       *
       */
      if (isset($_SESSION['badge_awarded']) && ($_SESSION['badge_awarded'] !== null)):
        global $DB;
        $badge = unserialize($_SESSION['badge_awarded']);
        $badgeObject = $DB->get_record('badge', array('id' => $badge->objectid));
        $image_url = moodle_url::make_pluginfile_url($badge->contextid, 'badges', 'badgeimage', $badge->objectid, '/', "f1", false);
        $_SESSION['badge_awarded'] = null;
    ?>

         <div id='badges_background' class='overlay'>
           <div id='badges_popup'>
             <img id="petites_etoiles" src='/theme/vital/pix/petites_etoiles_jaunes.png' style='display:none;width:30%;position:absolute;left:33%;' alt=''>
             <img id="grosses_etoiles" src='/theme/vital/pix/grosses_etoiles_jaunes.png' style='width:30%;position:absolute;left:33%;' alt=''>
             <img id="img_badge" src='<?php echo $image_url; ?>' style='width:30%;position:absolute;left:33%;' alt='' title='<?php echo $badgeObject->name; ?>'>
             <div id='text_badge' style='position:absolute;top:-30%;width:100%;'><h1 style='color:white;font-size:2vw;text-align:center;'>Tu as gagné un badge !</h1></div>
           </div>
         </div>

         <script>
             $('.overlay').css({'display' : 'block'})
             var popupMaterialTopOrigin = ($("#badges_background").height() - $("#badges_popup").height()) / 2
             var popupMaterialLeftOrigin = ($("#badges_background").width() - $("#badges_popup").width()) / 2
             $("#badges_popup").animate({
               "position": "absolute",
               "top": (popupMaterialTopOrigin * 2 + $("#badges_popup").height()) + 'px',
               "left" : popupMaterialLeftOrigin + "px"
             }, 0, function(){
               $("#badges_popup").animate({
                 "position": "absolute",
                 "top": (popupMaterialTopOrigin) + 'px',
                 "left" : popupMaterialLeftOrigin + "px"
               },
               1000, function(){
                 $("#grosses_etoiles").animate({
                   "position": "absolute",
                   "top": '-30%',
                   "left" : ($("#badges_popup").width() * 0.10) + "px",
                   "width" : "70%"
                 },
                 300, function(){
                   $("#petites_etoiles").css({
                     "position": "absolute",
                     "top": '-30%',
                     "left" : ($("#badges_popup").width() * 0.10) + "px",
                     "width" : "70%",
                     "display" : "block"
                   })
                   $("#petites_etoiles").animate({
                     "position": "absolute",
                     "top": '-30%',
                     "left" : ($("#badges_popup").width() * 0.05) + "px",
                     "width" : "75%",
                     "display" : "block"
                   }, 1450)
                   setTimeout(function(){$("#grosses_etoiles").attr('src', '/theme/vital/pix/grosses_etoiles_jaunes2.png' )}, 100)
                   setTimeout(function(){$("#grosses_etoiles").attr('src', '/theme/vital/pix/grosses_etoiles_jaunes3.png' )}, 250)
                   setTimeout(function(){$("#grosses_etoiles").attr('src', '/theme/vital/pix/grosses_etoiles_jaunes4.png' )}, 400)
                   setTimeout(function(){$("#grosses_etoiles").attr('src', '/theme/vital/pix/grosses_etoiles_jaunes.png' )}, 550)
                   setTimeout(function(){$("#grosses_etoiles").attr('src', '/theme/vital/pix/grosses_etoiles_jaunes3.png' )}, 700)
                   setTimeout(function(){$("#grosses_etoiles").attr('src', '/theme/vital/pix/grosses_etoiles_jaunes2.png' )}, 850)
                   setTimeout(function(){$("#grosses_etoiles").attr('src', '/theme/vital/pix/grosses_etoiles_jaunes.png' )}, 1000)
                   setTimeout(function(){$("#grosses_etoiles").attr('src', '/theme/vital/pix/grosses_etoiles_jaunes3.png' )}, 1150)
                   setTimeout(function(){$("#grosses_etoiles").attr('src', '/theme/vital/pix/grosses_etoiles_jaunes4.png' )}, 1300)
                   setTimeout(function(){$("#grosses_etoiles").attr('src', '/theme/vital/pix/grosses_etoiles_jaunes.png' )}, 1450)
                   setTimeout(function(){$("#grosses_etoiles").fadeOut();$("#petites_etoiles").fadeOut()}, 1600)
                   setTimeout(function(){
                     $("#text_badge").fadeOut()
                     $("#img_badge").animate({
                       "width" : "20px",
                       "left" : ($("#badges_popup").width() - 20) / 2
                     }, 1000)

                     $("#badges_popup").animate({
                       "position": "absolute",
                       "top": '0px',
                       "left" : ($("#badges_background").width() - $("#badges_popup").width() + ($("#badges_popup").width() - 20) / 2) + "px"
                     },
                     1000, function() {
                       $("#badges_background").fadeOut()
                       $("#elea_user_profil").parent().append($("#img_badge"))
                       $("#img_badge").css({
                         'position' : 'static',
                         'width' : '20px',
                         'margin-left' : '10px'
                       })
                     })
                   }, 2000)
                 })
               })
             })
         </script>


    <?php
      endif;
     ?>

    <?php echo $OUTPUT->standard_end_of_body_html() ?>

</div>
    <!-- Classie - class helper functions by @desandro https://github.com/desandro/classie -->
    <!-- <script src="../theme/vital/layout/js/classie.js"></script> -->
    <script>
        $('.lessonbutton:contains(Retour à)').css({'display' : 'none'})
        var showRight = document.getElementById( 'showRight' );
        var menubody = document.getElementById( 'menubody' );

        showRight.onclick = function() {
          classie.toggle( this, 'active' );
          classie.toggle( menubody, 'open' );
        };
    </script>
</body>
</html>
