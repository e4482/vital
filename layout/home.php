<?php
// This file is part of The Bootstrap 3 Moodle theme
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG;

$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = false;

$knownregionpre = $PAGE->blocks->is_known_region('side-pre');
$knownregionpost = false;

$regions = vital_grid($hassidepre, $hassidepost);
$PAGE->set_popup_notification_allowed(false);
$setzoom = 0;
echo $OUTPUT->doctype() ?>

<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
      <title><?php echo theme_vital_get_hostname(); ?></title>
      <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
      <?php echo $OUTPUT->standard_head_html(); ?>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
    </head>
    <body <?php echo $OUTPUT->body_attributes($setzoom); ?>>
      <?php echo $OUTPUT->standard_top_of_body_html() ?>
      <div class="logos">
          <div class="marque-etat">
              <a href='http://www.ac-versailles.fr'>
                  <img src='<?php echo $OUTPUT->image_url('ac_versailles', 'theme'); ?>' alt='académie de Versailles'>
              </a>
          </div>
          <div class="logo-dane">
              <a href='http://www.dane.ac-versailles.fr'>
                  <img src='<?php echo $OUTPUT->image_url('dane_versailles', 'theme'); ?>' alt='DANE de Versailles'>
              </a>
          </div>
      </div>
      <div class="illu">
        <div class="headerIllu">
          <div style="position:absolute;width:100%;"><h2 class="headerLogo">Éléa, plateforme d'e-éducation de l'académie de Versailles</h2></div>
          <figure class="grass-2plan"></figure>
          <figure class="grass-3plan-left"></figure>
          <figure class="grass-3plan-right"></figure>
          <figure class="sky-4plan-left"></figure>
          <figure class="sky-4plan-right"></figure>
        </div>
      </div>
      <section class="stats2">
        <div id="showcase">
          <h2 class="col-xs-12">Plateforme d'e-éducation de l'académie de Versailles</h2>
          <h4><?php echo (isset($CFG->elea_version)) ? $CFG->elea_version : "version inconnue"; ?></h4>
          <ul class="buttons-homepage col-sm-8 col-sm-offset-2  col-md-4 col-md-offset-4">
            <li><a href="login/index.php"><span>Démarrer</span></a></li>
            <li><a href="local/faq/index.php"><span>Tutoriels</span></a></li>
            <li><a href="local/apropos/"><span>À propos</span></a></li>
          </ul>
        </div>
      </section>

      <!-- <div><?php echo $OUTPUT->main_content(); ?></div> -->

    </body>
</html>
