<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme vital lib.
 *
 * @package     theme_vital
 * @author      2017 Pascal Fautrero
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


class navigationModule {

    protected $courseid;
    protected $current_module_type;
    protected $current_module_id;
    protected $current_course_module;
    protected $section_parent;

    /***************************************************
     *
     * return void
     *
     * *************************************************/
    public function __construct($courseid, $moduletype, $moduleid) {

        $this->courseid = $courseid;
        $this->current_module_type = $moduletype;
        $this->current_module_id = $moduleid;

    }

    /***************************************************
     *
     * return @array
     *
     * *************************************************/
    public function permissions($pageurl) {

      $allow_nextModule = true;
      $allow_previousModule = true;

      if ($this->current_module_type == "lesson"){
        $pageid = preg_match("/(.*)\/mod\/lesson\/view.php\?id=([0-9]*)&amp;pageid=(-?[0-9]*)/",
          $pageurl,
          $results);
        if ($pageid) {
          if ($results[3] != '-9') {
            global $DB;
            $currentpage = $DB->get_record("lesson_pages", array('id' => $results[3]));
            if ($currentpage->prevpageid != '0') {
              $allow_nextModule = false;
              $allow_previousModule = false;
            }

          }
        }
      }
      return array($allow_previousModule, $allow_nextModule);
    }


    /***************************************************
     *
     * return @array
     *
     * *************************************************/
    public function get_modules_list() {
      global $DB;
      $course_sections = $DB->get_records_sql("
          SELECT id, sequence
          FROM {course_sections}
          WHERE course = '" . $this->courseid . "'
          ORDER BY section ASC");
      $global_sequence = "";
      $final_array = [];
      foreach($course_sections as $course_section) {
          $global_sequence .= $course_section->sequence . ",";
      }
      $course = $DB->get_record('course',array('id' => $this->courseid));
      $modinfo = get_fast_modinfo($course);
      $modules_array = explode(",", $global_sequence);
      foreach ($modules_array as $cmid) {
        if ($cmid) {
          $cm = $modinfo->get_cm($cmid);
          if (($cm->visible) && ($cm->uservisible)) {
            array_push($final_array, $cmid);
          }
        }
      }

      $this->current_course_module = $DB->get_record_sql("
          SELECT cm.id
          FROM {course_modules} as cm, {modules} as m
          WHERE cm.course = '" . $this->courseid . "'
          AND cm.module = m.id
          AND m.name = '" . $this->current_module_type . "'
          AND cm.instance = " . $this->current_module_id . "
      ");

      return $final_array;

    }
    public function get_parent_section($course_module_id) {

      global $DB;
      $parent_section = null;
      $course_sections = $DB->get_records_sql("
          SELECT id, sequence, section
          FROM {course_sections}
          WHERE course = '" . $this->courseid . "'
          ORDER BY section ASC");
      $global_sequence = "";
      foreach($course_sections as $course_section) {
          $sequence_array = explode(",", $course_section->sequence);
          if (in_array($course_module_id, $sequence_array)) {
              $parent_section = $course_section->section;
          }
      }
      return $parent_section;

    }
    /****************************************************
     *
     *
     *
     * **************************************************/

    public function html() {

      global $DB;

      $global_sequence_array = $this->get_modules_list();
      $nextModule = "";
      $previousModule = "";
      $key = array_search($this->current_module_id, $global_sequence_array);
      $css_style = 'float:right;';
      if ($this->current_module_type == 'lti') {
          $css_style .= 'position:relative;top:-80px;';
      }
      if ($key !== false) {
        if (array_key_exists($key + 1, $global_sequence_array) &&
            $global_sequence_array[$key + 1] != '') {
            $next_course_module = $DB->get_record_sql("
                SELECT cm.instance, m.name
                FROM {course_modules} as cm, {modules} as m
                WHERE cm.id = " . $global_sequence_array[$key + 1] . "
                AND m.id = cm.module
            ");
            if ($next_course_module->name != 'label') {
                $nextModule = "<a style='" . $css_style . "' href='/mod/"
                    . $next_course_module->name
                    . "/view.php?id="
                    . $global_sequence_array[$key + 1]
                    . "'><img src='/theme/vital/pix/next.png' alt='next' style='margin:5px;' /></a>";
            }
            else {
                $nextModule = "<a style='" . $css_style . "' href='/course/"
                    . "view.php?id="
                    . $this->courseid
                    . "&section="
                    . $this->get_parent_section($global_sequence_array[$key + 1])
                    . "'><img src='/theme/vital/pix/next.png' style='margin:5px;' alt='next' /></a>";
            }
          }
          else {
              $nextModule = "<a style='" . $css_style . "' href='/local/teacherboard/index.php'>
              <img src='/theme/vital/pix/home.png' alt='next' style='margin:5px;' /></a>";
          }
          if (array_key_exists($key - 1, $global_sequence_array) &&
              $global_sequence_array[$key - 1] != '') {
              $next_course_module = $DB->get_record_sql("
                  SELECT cm.instance, m.name
                  FROM {course_modules} as cm, {modules} as m
                  WHERE cm.id = " . $global_sequence_array[$key - 1] . "
                  AND m.id = cm.module
              ");
              if ($next_course_module->name != 'label') {
                  $previousModule = "<a style='" . $css_style . "' href='/mod/"
                      . $next_course_module->name
                      . "/view.php?id="
                      . $global_sequence_array[$key - 1]
                      . "'><img src='/theme/vital/pix/previous.png' style='margin:5px;' alt='previous' /></a>";
              }
              else {
                  $previousModule = "<a style='" . $css_style . "' href='/course/"
                      . "view.php?id="
                      . $this->courseid
                      . "&section="
                      . $this->get_parent_section($global_sequence_array[$key - 1])
                      . "'><img src='/theme/vital/pix/previous.png' alt='previous' style='margin:5px;' /></a>";
              }
          }
      }
      return array($previousModule,$nextModule);
    }
}



/***************************************************
 *
 * Used to override jquery ui css
 *
 * *************************************************/
function theme_vital_page_init(moodle_page $page) {
    // There is no need to $page->requires->jquery() if the theme does not use jQuery.
    $page->requires->jquery_override_plugin('ui-css', 'elea-jqueryui-css');
}





function theme_vital_get_hostname() {

    global $CFG;
    $hostname = "moodle";
    $found = preg_match("/https?:\/\/(.*)\.elea\.ac-versailles\.fr/",
        $CFG->wwwroot,
        $results);

    if ($found) {
        $hostname = $results[1];
    }
    return "elea " . $hostname;
}


function theme_vital_process_css($css, $theme) {

    // Set the background image for the logo.
    $logo = $theme->setting_file_url('logo', 'logo');
    $css = theme_vital_set_logo($css, $logo);

    // Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_vital_set_customcss($css, $customcss);

    return $css;
}


function theme_vital_set_logo($css, $logo) {
    $logotag = '[[setting:logo]]';
    $logoheight = '[[logoheight]]';
    $logowidth = '[[logowidth]]';
    $logodisplay = '[[logodisplay]]';
    $width = '0';
    $height = '0';
    $display = 'none';
    $replacement = $logo;
    if (is_null($replacement)) {
        $replacement = '';
    } else {
        $dimensions = getimagesize('http:'.$logo);
        $width = $dimensions[0] . 'px';
        $height = $dimensions[1] . 'px';
        $display = 'block';
    }
    $css = str_replace($logotag, $replacement, $css);
    $css = str_replace($logoheight, $height, $css);
    $css = str_replace($logowidth, $width, $css);
    $css = str_replace($logodisplay, $display, $css);

    return $css;
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_vital_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    if ($context->contextlevel == CONTEXT_SYSTEM && ($filearea === 'logo')) {
        $theme = theme_config::load('vital');
        return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
    } else {
        send_file_not_found();
    }
}

/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_vital_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

function vital_grid($hassidepre, $hassidepost) {

    $regions = Array();
    if ($hassidepre && $hassidepost) {
        //$regions = array('content' => 'col-sm-6 col-sm-push-3 col-lg-8 col-lg-push-2');
        $regions['pre'] = 'col-sm-3 col-sm-pull-6 col-lg-2 col-lg-pull-8';
        $regions['post'] = 'col-sm-3 col-lg-2';
    } else if ($hassidepre && !$hassidepost) {
        //$regions = array('content' => 'col-sm-9 col-sm-push-3 col-lg-10 col-lg-push-2');
        $regions['pre'] = 'col-sm-3 col-sm-pull-9 col-lg-2 col-lg-pull-10';
        $regions['post'] = 'emtpy';
    } else if (!$hassidepre && $hassidepost) {
        //$regions = array('content' => 'col-sm-9 col-lg-10');
        $regions['pre'] = 'empty';
        $regions['post'] = 'col-sm-3 col-lg-2';
    } else if (!$hassidepre && !$hassidepost) {
        //$regions = array('content' => 'col-md-12');
        $regions['pre'] = 'empty';
        $regions['post'] = 'empty';
    }

    // define content array
    $regions['content'] = 'col-sm-12 col-lg-12';
    $regions['pre'] = 'empty';


    if ('rtl' === get_string('thisdirection', 'langconfig')) {
        if ($hassidepre && $hassidepost) {
            $regions['pre'] = 'col-sm-3  col-sm-push-3 col-lg-2 col-lg-push-2';
            $regions['post'] = 'col-sm-3 col-sm-pull-9 col-lg-2 col-lg-pull-10';
        } else if ($hassidepre && !$hassidepost) {
            $regions = array('content' => 'col-sm-9 col-lg-10');
            $regions['pre'] = 'col-sm-3 col-lg-2';
            $regions['post'] = 'empty';
        } else if (!$hassidepre && $hassidepost) {
            $regions = array('content' => 'col-sm-9 col-sm-push-3 col-lg-10 col-lg-push-2');
            $regions['pre'] = 'empty';
            $regions['post'] = 'col-sm-3 col-sm-pull-9 col-lg-2 col-lg-pull-10';
        }
    }
    return $regions;
}

/**
 * Loads the JavaScript for the zoom function.
 *
 * @param moodle_page $page Pass in $PAGE.
 */
function theme_vital_initialise_zoom(moodle_page $page) {
    user_preference_allow_ajax_update('theme_bootstrap_zoom', PARAM_TEXT);
    $page->requires->yui_module('moodle-theme_bootstrap-zoom', 'M.theme_bootstrap.zoom.init', array());
}

/**
 * Get the user preference for the zoom function.
 */
function theme_vital_get_zoom() {
    return get_user_preferences('theme_bootstrap_zoom', '');
}
