<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Group observers.
 *
 * @package    mod_quiz
 * @copyright  2013 Frédéric Massart
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace theme_vital;
defined('MOODLE_INTERNAL') || die();

//require_once($CFG->dirroot . '/mod/quiz/locallib.php');

/**
 * Group observers class.
 *
 * @package    theme_vital
 * @author     Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class group_observers {

    /**
     * A badge is awarded
     *
     * @param \core\event\base $event The event.
     * @return void
     */
    public static function badge_awarded($event) {
        error_log('theme_vital observer : badge_awarded !');
        $_SESSION['badge_awarded'] = serialize($event);
    }
}
