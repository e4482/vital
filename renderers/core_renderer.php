<?php
// This file is part of The Bootstrap 3 Moodle theme
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Renderers to align Moodle's HTML with that expected by Bootstrap
 *
 * @package    theme_bootstrap
 * @copyright  2012
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class theme_vital_core_renderer extends core_renderer {

    public function notification($message, $classes = 'notifyproblem') {
        $message = clean_text($message);

        if ($classes == 'notifyproblem') {
            return html_writer::div($message, 'alert alert-danger');
        }
        if ($classes == 'notifywarning') {
            return html_writer::div($message, 'alert alert-warning');
        }
        if ($classes == 'notifysuccess') {
            return html_writer::div($message, 'alert alert-success');
        }
        if ($classes == 'notifymessage') {
            return html_writer::div($message, 'alert alert-info');
        }
        if ($classes == 'redirectmessage') {
            return html_writer::div($message, 'alert alert-block alert-info');
        }
        if ($classes == 'notifytiny') {
            // Not an appropriate semantic alert class!
            return $this->debug_listing($message);
        }
        return html_writer::div($message, $classes);
    }

    private function debug_listing($message) {
        $message = str_replace('<ul style', '<ul class="list-unstyled" style', $message);
        return html_writer::tag('pre', $message, array('class' => 'alert alert-info'));
    }

    public function navbar() {
        global $CFG;

        $items = $this->page->navbar->get_items();
        if (empty($items)) { // MDL-46107
            return '';
        }
        $breadcrumbs = '';

        foreach ($items as $item) {
            
            // Le noeud racine Tableau de bord est remplacé par Mon espace
            if ($item->key === 'myhome') {
                $item->text = "Mon espace";
                $item->action = new moodle_url('/local/teacherboard/index.php');
            }
            
            // Le noeud Cours (ou Mes Cours) est supprimé
            else if ($item->key === 'mycourses' || $item->key === 'courses') {
                continue;
            }
            
            // Le noeud au nom de la catégorie du cours est supprimé
            else if (preg_match("/\/course\/index.php\?categoryid=/", $item->action)) {
                continue;
            }
            
            // On ne veut surtout pas voir le nom abrégé d'un cours mais son nom complet
            else if ($item->title != "") {
                $item->text = $item->title;
            }
            
            // On ne veut pas non plus voir d'icône apparaitre
            $item->hideicon = true;
            
            $breadcrumbs .= "<li>" . $this->render($item) . "</li>";
            
        }
        
        if ($breadcrumbs) {
            return "<ol class=breadcrumb>$breadcrumbs</ol>";
        }
        return "";
    }

    public function custom_menu($custommenuitems = '') {
        // The custom menu is always shown, even if no menu items
        // are configured in the global theme settings page.
        global $CFG;

        if (empty($custommenuitems) && !empty($CFG->custommenuitems)) { // MDL-45507
            $custommenuitems = $CFG->custommenuitems;
        }
        $custommenu = new custom_menu($custommenuitems, current_language());
        return $this->render_custom_menu($custommenu);
    }

    protected function render_custom_menu(custom_menu $menu) {
        global $CFG, $USER;

        // TODO: eliminate this duplicated logic, it belongs in core, not
        // here. See MDL-39565.

        $content = '<ul class="nav navbar-nav">';
        foreach ($menu->get_children() as $item) {
            $content .= $this->render_custom_menu_item($item, 1);
        }

        return $content.'</ul>';
    }

    public function user_menu($user = NULL, $withlinks = NULL) {
        global $CFG;
        $usermenu = new custom_menu('', current_language());
        return $this->render_user_menu($usermenu);
    }

    protected function render_user_menu(custom_menu $menu) {
        global $CFG, $USER, $DB, $PAGE, $COURSE;

        $addusermenu = true;
        $addlangmenu = false;


        if (isloggedin()) {
            $menu->add(
                '<span style="padding-right:5px;" class="glyphicon glyphicon-off"></span>' . get_string('logout'),
                new moodle_url('/login/logout.php', array('sesskey' => sesskey(), 'alt' => 'logout')),
                get_string('logout')
            );

        } else {
            //$menu->add(get_string('login'), new moodle_url('/login/index.php'), get_string('login'), 10001);
        }


        if ($addusermenu) {
            if (isloggedin()) {
                //$usermenu = $menu->add(fullname($USER), new moodle_url('#'), fullname($USER), 10001);
                /*$usermenu = $menu->add(
                    '<span class="glyphicon glyphicon-cog"></span> ' . get_string('editmyprofile'),
                    new moodle_url('/user/edit.php', array('id' => $USER->id)),
                    get_string('editmyprofile')
                );*/


             if (($PAGE->course->id == 1) && isset($_SESSION['elea_menu_previous_page']) && $_SESSION['elea_menu_previous_page'] != "") {
                 $usermenu = $menu->add(
                   '<span class="glyphicon glyphicon-chevron-left"></span> Revenir au parcours',
                   new moodle_url($_SESSION['elea_menu_previous_page']),
                   "revenir au parcours"
                 );
                 $usermenu->add(
                   '<span id="elea_user_profil" class="glyphicon glyphicon-user"></span> Mon Profil',
                   new moodle_url('/user/profile.php', array('id' => $USER->id)),
                   get_string('viewprofile')
                 );
                 
             }
             else {
                 $usermenu = $menu->add(
                   '<span id="elea_user_profil" class="glyphicon glyphicon-user"></span> Mon Profil',
                   new moodle_url('/user/profile.php', array('id' => $USER->id)),
                   get_string('viewprofile')
                 );
                 $usermenu = $menu->add(
                   '<span id="elea_user_role" class="glyphicon glyphicon-user"></span> Prendre le rôle de...',
                   new moodle_url('/course/switchrole.php', array('id' => $COURSE->id, 'returnurl' => $PAGE->url, 'switchrole' => -1)),
                   'changer de rôle'
                   
                 );
                 if (isset($COURSE->id) && $COURSE->id != 1) {
                     $usermenu = $menu->add(
                         '<span class="glyphicon glyphicon-tasks"></span> Compétences',
                         new moodle_url('/admin/tool/lp/coursecompetencies.php', array('courseid' => $COURSE->id)),
                         'Compétences'

                     );
                 }
             }

            $usermenu->add(
                '<span class="glyphicon elea-my-grades"></span> Mes Notes',
                new moodle_url('/grade/report/overview/index.php'),
                get_string('viewprofile')
            );

             if ($PAGE->course->id == 1) {
                 //$_SESSION['elea_menu_previous_page'] = "";
                 $usermenu->add(
                   '<span class="elea-my-messages"></span> Mes Messages',
                   new moodle_url('/message/index.php'),
                   "Mes Messages"
                 );
             }
             else {
                 $_SESSION['elea_menu_previous_page'] = $PAGE->url;
             }

            } else {
                $usermenu = $menu->add(get_string('login'), new moodle_url('/login/index.php'), get_string('login'), 10001);
            }
        }

        $content = '<ul class="nav navbar-nav navbar-right">';
        foreach ($menu->get_children() as $item) {
            $content .= $this->render_custom_menu_item($item, 1);
        }

        return $content.'</ul>';
    }
    protected function render_tabtree(tabtree $tabtree) {
        if (empty($tabtree->subtree)) {
            return '';
        }
        $firstrow = $secondrow = '';
        foreach ($tabtree->subtree as $tab) {
            $firstrow .= $this->render($tab);
            if (($tab->selected || $tab->activated) && !empty($tab->subtree) && $tab->subtree !== array()) {
                $secondrow = $this->tabtree($tab->subtree);
            }
        }
        return html_writer::tag('ul', $firstrow, array('class' => 'nav nav-tabs nav-justified')) . $secondrow;
    }

    protected function render_tabobject(tabobject $tab) {
        if ($tab->selected or $tab->activated) {
            return html_writer::tag('li', html_writer::tag('a', $tab->text), array('class' => 'active'));
        } else if ($tab->inactive) {
            return html_writer::tag('li', html_writer::tag('a', $tab->text), array('class' => 'disabled'));
        } else {
            if (!($tab->link instanceof moodle_url)) {
                // Backward compatibility when link was passed as quoted string.
                $link = "<a href=\"$tab->link\" title=\"$tab->title\">$tab->text</a>";
            } else {
                $link = html_writer::link($tab->link, $tab->text, array('title' => $tab->title));
            }
            return html_writer::tag('li', $link);
        }
    }

    public function box($contents, $classes = 'generalbox', $id = null, $attributes = array()) {
        if (isset($attributes['data-rel']) && $attributes['data-rel'] === 'fatalerror') {
            return html_writer::div($contents, 'alert alert-danger', $attributes);
        }
        return parent::box($contents, $classes, $id, $attributes);
    }

    public function content_zoom() {
        //$zoomin = html_writer::span(get_string('fullscreen', 'theme_bootstrap'), 'zoomin');
        //$zoomout = html_writer::span(get_string('closefullscreen', 'theme_bootstrap'), 'zoomout');
        //$content = html_writer::link('#',  $zoomin . $zoomout, array('class' => 'btn btn-default pull-right moodlezoom'));
        $content = "";
        return $content;
    }
}
