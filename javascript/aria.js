// This file is part of vital
//
// vital is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// vital is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

//@author : remi.lefeuvre@ac-versailles.fr, wahid.mendil@crdp.ac-versailles.fr
//          pascal.fautrero@ac-versailles.fr


// item1 --- subitem11 --- subitem111
//   |
//   ------- subitem12 --- subitem121
//
// item2 --- subitem21
//   |
//   ------- subitem22
//
// case 0 : item1
// case 1 : item1 - item1
// case 2 : item1 - item2
// case 3 : item1 - subitem11 - subitem12
// case 4 : item1 - subitem11 - subitem11
// case 5 : item1 - subitem11 - item2
// case 6 : item1 - subitem11 - item1
// case 7 : item1 - subitem11
// case 8 : item1 - subitem11 - subitem111
// case 9 : item1 - subitem11 - subitem111 - subitem111
// case 10: item1 - subitem11 - subitem111 - subitem11
// case 11: item1 - subitem11 - subitem111 - subitem12
// case 12: item1 - subitem11 - subitem111 - item1
// case 13: item1 - subitem11 - subitem111 - item2


YUI().use("node-base", function(Y){
    Y.on("domready", function(){
        /*
         * Debug Object
         */
        var DebugObject = function() {
            this.active = true;
        };
        DebugObject.prototype.log = function(msg) {
            if (this.active) {
                console.log(msg);
            }
        };
        /*
         * Menu Object
         */
        var MenuObject = function(config) {
            this.state = 0;
            this.previousEl = null;
            this.event = "";
            this.ELEMENT_NODE = 1;
            this.gap = "300px";
            this.top = 64;
            this.debug = config.debug
            this.handler = config.handler
            this.menulevel2 = config.menulevel2
            
        };

        MenuObject.prototype.subBlockToRight2 = function(el, gap, color) {
			var that=this
			require(['jquery'], function($) {			
				var ul = el.parent().find("ul")
				ul.css({
					'left' : gap, 
					//'top' : that.top - that.rootParentOld(el.parent()[0]).getBoundingClientRect().top + "px",
					'top' : "0px",
					'background-color' : color
				})
				if (gap == "0px") {
					ul.attr("aria-hidden", 'true')
					ul.hide()
					el.attr("aria-expanded", 'false')
					el.attr("aria-selected", 'false')
				}
				else {
					el.parent().children("ul").show()
				}
			})
        }

        MenuObject.prototype.rootParentOld = function(liElement) {
            var ulElement = liElement.parentNode;
            if (ulElement.className == "") {
                ulElement = ulElement.parentNode.parentNode;
                liElement = liElement.parentNode.parentNode;
                if (ulElement.className == "") {
                    ulElement = ulElement.parentNode.parentNode;
                    liElement = liElement.parentNode.parentNode;
                }
            }
            return liElement;
        }

		MenuObject.prototype.rootParent = function(el) {
		
			var liRoot = null
		
			if (el.parent().hasClass('block_tree')) {
				liRoot = el
			}
			else {
				liRoot = el.parent()
			}
		
			while(!liRoot.parent().hasClass('block_tree')) {
				liRoot = liRoot.parent().parent() 
			}			
			return liRoot.children('p')
		}

		MenuObject.prototype.subRootParent = function(el) {
		
			var liSubRoot = el.parent()
		
			if (liSubRoot.parent().hasClass('block_tree')) {
				return el
			}
		
			while(!liSubRoot.parent().parent().parent().hasClass('block_tree')) {
				liSubRoot = liSubRoot.parent().parent()
			}			
			return liSubRoot.children('p')
		}



      
        MenuObject.prototype.sameRoot = function(el1, el2) {
		
			var root1 = null
			var root2 = null
		
			if (el1.parent().hasClass('block_tree')) {
				root1 = el1
			}
			else {
				root1 = el1.parent()
			}
			if (el2.parent().hasClass('block_tree')) {
				root2 = el2
			}
			else {
				root2 = el2.parent()
			}
			
			while(!root1.parent().hasClass('block_tree')) {
				root1 = root1.parent().parent() 
			}
			while(!root2.parent().hasClass('block_tree')) {
				root2 = root2.parent().parent() 
			}			
			
			return root1.is(root2)
			
		}

        MenuObject.prototype.moveToLeft2 = function(el, evt) {
			var that = this
			require(['jquery'], function($) {
				
				if ($(that.handler).hasClass('cbp-spmenu-push-toleft')) {
					if ((! el.is(that.previousEl)) && (that.previousEl != null)) {
						that.previousEl.parent().addClass("collapsed")

						if (!el.parent().parent().hasClass('block_tree')) {

							// user clicked on subitem

							if (that.previousEl.parent().parent().hasClass('block_tree')) {
								// case 7 : item1 - subitem11
								that.debug.log('case 7')
								$(that.menulevel2).addClass('push-toleft')
								that.subBlockToRight2(el, that.gap, '#2DA1BD')
							}
							else {
								if (!el.parent().parent().parent().parent().hasClass('block_tree')) {
									// case 8 : item1 - subitem11 - subitem111
									that.debug.log('case 8')
									if (el.attr('aria-expanded') == 'true') {
										el.parent().children('ul').show()
									}
									else {
										el.parent().children('ul').hide()
									}
									el.parent().children('ul').css({
											'left' : '0px', 
											'top' : '0px',
											'background-color' : '#1D91AD'
									})

								}
								else {

									if (that.previousEl.parent().parent().parent().is(el.parent())) {
										// case 10: item1 - subitem11 - subitem111 - subitem11
										that.debug.log('case 10')
										$(that.menulevel2).removeClass('push-toleft')
										that.subBlockToRight2(that.previousEl, "0px", '#2DA1BD')
										that.subBlockToRight2(el, that.gap, '#2DA1BD')
									}
									else if (that.previousEl.parent().parent().is(el.parent().parent())) {
										// case 3 : item1 - subitem11 - subitem12
										that.debug.log('case 3')
										$(that.menulevel2).addClass('push-toleft')
										that.subBlockToRight2(that.previousEl, "0px")
										that.subBlockToRight2(el, that.gap, '#2DA1BD')						
									}
									else {
										// case 11: item1 - subitem11 - subitem111 - subitem12										
										that.debug.log('case 11')
										$(that.menulevel2).addClass('push-toleft')
										that.subBlockToRight2(that.previousEl, "0px", '#2DA1BD')
										that.subBlockToRight2(that.previousEl.parent().parent().parent().children('p'), "0px", '#2DA1BD')
										that.previousEl.parent().addClass('collapsed')
										that.subBlockToRight2(el, that.gap, '#2DA1BD')										
									}
								}
							}
						}
						else if (el.parent().parent().hasClass('block_tree') && 
							 (that.sameRoot(that.previousEl, el))) {

							//user clicked on root item in the same branch than previous item

							if (!that.previousEl.parent().parent().parent().is(el.parent())) {
								// case 12: item1 - subitem11 - subitem111 - item1								
								that.debug.log("case 12")
								that.previousEl.parent().addClass('collapsed')
								$(that.menulevel2).removeClass('push-toleft')
								$(that.handler).removeClass('cbp-spmenu-push-toleft')
								that.subBlockToRight2(that.previousEl, "0px", '#2DA1BD');								
							}
							else {
								// case 6 : item1 - subitem11 - item1
								that.debug.log("case 6")
								$(that.menulevel2).removeClass('push-toleft')
								$(that.handler).removeClass('cbp-spmenu-push-toleft')
								that.subBlockToRight2(that.previousEl, "0px", '#2DA1BD')                         								
							}
								
						}
					
						else if (el.parent().parent().hasClass('block_tree') && 
							(!that.previousEl.parent().parent().parent().is(el.parent()))) {

							//user clicked on root item in a new branch

							if (that.previousEl.parent().parent().hasClass('block_tree')) {
								// case 2 : item1 - item2
								that.debug.log("case 2")
								that.previousEl.parent().addClass("collapsed")
								that.subBlockToRight2(that.previousEl, "0px", '#2DA1BD');
								that.subBlockToRight2(el, that.gap, '#36ABC6');
							}
							else if (that.previousEl.parent().parent().parent().parent().hasClass('block_tree')){
								// case 5 : item1 - subitem11 - item2
								that.debug.log("case 5")
								$(this.menulevel2).removeClass('push-toleft')
								that.previousEl.parent().addClass("collapsed")
								that.subBlockToRight2(that.previousEl, "0px", '#2DA1BD')
								that.subBlockToRight2(that.previousEl.parent().parent().parent().children('p'), "0px")
								that.subBlockToRight2(el, that.gap, '#36ABC6');

							}
							else {
								// case 13: item1 - subitem11 - subitem111 - item2
								that.debug.log("case 13")
								that.rootParent(that.previousEl).addClass('collapsed')
								$(this.menulevel2).removeClass('push-toleft')
								that.previousEl.parent().addClass('collapsed')								
								that.subBlockToRight2(that.previousEl.parent().parent().parent().children('p'), "0px", '#2DA1BD')
								that.subBlockToRight2(that.previousEl, "0px", '#2DA1BD')
								that.subBlockToRight2(that.rootParent(that.previousEl), "0px", '#2DA1BD')
								that.subBlockToRight2(el, that.gap, '#36ABC6');
							}
							
						}
						that.previousEl = el

					}
					else {
						if (el.parent().parent().hasClass('block_tree')) {
							// case 1 : item1 - item1
							that.debug.log("case 1")
							$(this.menulevel2).removeClass('push-toleft')
							$(that.handler).removeClass('cbp-spmenu-push-toleft')
							that.subBlockToRight2(el, "0px", '#2DA1BD');                        
							that.previousEl = el;
						}
						else {
							if (el.parent().parent().parent().parent().hasClass('block_tree')) {
								// case 4 : item1 - subitem11 - subitem11
								that.debug.log("case 4")
								if (el.attr('aria-expanded') == 'true') {
									that.debug.log("case 4.1")
									that.subBlockToRight2(el, that.gap, '#2DA1BD');
									$(that.menulevel2).addClass('push-toleft')
								}
								else {
									that.debug.log("case 4.2")
									that.subBlockToRight2(el, "0px", '#2DA1BD');
									$(that.menulevel2).removeClass('push-toleft')
								}
							}
							else {
								// case 9 : item1 - subitem11 - subitem111 - subitem111
								that.debug.log("case 9")
								if (el.attr('aria-expanded') == 'true') {
									el.parent().children('ul').show()
								}
								else {
									el.parent().children('ul').hide()
								}
								el.parent().children('ul').css({'left' : '0px', 'top' : '0px'})
							}							
						
						}						
					}
				}
				else {
					// case 0 : item1
					that.debug.log("case 0")
					$(that.handler).addClass('cbp-spmenu-push-toleft')
					that.subBlockToRight2(el, that.gap, '#36ABC6');
					that.state++
					that.previousEl = el
				}

			});  			
		}
		
		var debug = new DebugObject()
		var menu = new MenuObject({
			'debug' : debug,
			'handler' : document.getElementById( 'cbp-spmenu-s2' ),
			'menulevel2' : document.getElementById( 'menulevel2' )
		})       
		var showRight = document.getElementById( 'showRight' )
        var menubody = document.getElementById( 'menubody' )

		if (menubody) {
			require(['jquery'], function($) {

				//$("#menubody").css({'position' : 'absolute'})
				$(document.body).css({'overflow-x' : 'hidden'})

				$(".tree_item").each(function(){
					if ($(this).parent().hasClass('contains_branch')) {
						$(this).parent().addClass('collapsed')
						$(this).on('click', function(e){
							if ($(this).parent().hasClass('type_siteadmin')) {
								if ($(this).parent().children('ul').length > 0) {
									menu.moveToLeft2($(this), e)
								}
								else {
									console.log("hmm...bad guy")
									menu.toto = e
								}
							}
							else {
								menu.moveToLeft2($(this), e)
							}
						})
						if ($(this).attr('aria-selected') == 'true') {
							$(this).attr('aria-selected', 'false')
							$(this).attr('aria-expanded', 'false')
						}
					}

				})
				
				var observer = new MutationObserver(function(mutations) {
					mutations.forEach(function(mutation) {
						var addedNode = mutation.addedNodes[0]
						$(addedNode).find('.tree_item').each(function(){
							if ($(this).parent().hasClass('contains_branch')) {
								$(this).on('click', function(e){
									menu.moveToLeft2($(this), e)
								})
							}
						})
						
						if ($(addedNode).parent().hasClass('type_siteadmin')) {
							console.log('lets go baby !!')
							menu.moveToLeft2($(addedNode).parent().children('p'), menu.toto)
						}						
						
						
					})
				})
				var config = { subtree: true, childList: true }
				observer.observe(menubody, config)




				$("#close_menu2").on('click tap',function(e) {
					menu.rootParent(menu.previousEl).click()
					// weird hide operation - needed because not managed with previous click...
					menu.rootParent(menu.previousEl).parent().children('ul').hide()
					
				})

				$("#close_menu1").on('click tap',function(e) {
					menu.subRootParent(menu.previousEl).click()
				})

				$("#showRight").on('click tap',function() {
					$(menubody).find('.contains_branch').each(function(){
						$(this).children('p').attr('aria-selected', 'false')
						$(this).children('p').attr('aria-expanded', 'false')
						$(this).children('ul').attr('aria-hidden', 'false')
						$(this).children('ul').hide()
						$(menu.handler).removeClass('cbp-spmenu-push-toleft')
						$(menu.menulevel2).removeClass('push-toleft')
					})
				})			
			})
		}      
    })
})
