/**
 * Use to retrieve book specific block and place it at the right place
 */
var bookblock = $('.block_book_toc')
if (bookblock.length > 0 && bookblock.find('.action-list').length > 0) {
    bookblock.appendTo( $('#region-main') )
    bookblock.find('h2').css('font-size', 24)
}