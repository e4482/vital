/**
 * Use to retrieve quiz timer and place it at the right place
 */
var quiztimer = $('#quiz-timer')
if (quiztimer) {
    quiztimer.appendTo( $('#region-main') )
    quiztimer.css({
        'margin-top': '50px',
        'font-size': 'large'
    })
}

var block = $('#mod_quiz_navblock');
if (block) {
    block.hide()
}
