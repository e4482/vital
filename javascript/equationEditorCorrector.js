/**
 * Script to correct elea/platform#607:_Activité Test : les boutons bleus n'apparaissent pas dans l'éditeur d'équation
 * en mode réponse pour une question composition
 *
 * The error is due to an exception thrown by lib functions when envounter an element with a href containing the ":"
 * character.
 *
 * This script rename these elements.
 **/

function rename(oldName) {
    let regExTag = /q[0-9]*\:/
    let tag = regExTag.exec(oldName)[0]
    let newTag = tag.replace(":", "_")
    return oldName.replace(tag, newTag)
}

function renameHref () {
    // Nav items rename
    document.querySelectorAll('form.atto_form div.atto_equation_library a.nav-link').forEach(
        element => element.href = rename(element.href))

    // Targetted elements
    document.querySelectorAll('form.atto_form div.tab-pane').forEach(
        element => element.id = rename(element.id))
}

function defferedRename() {
    setTimeout(renameHref, 1000)
}

function deferredOnClick() {
    document.querySelectorAll('button.atto_equation_button').forEach(
        element => element.addEventListener('click', defferedRename)
    )
}

setTimeout(deferredOnClick, 2000)
