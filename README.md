# VITAL

-----

## Requirements

### Parent theme

Install theme_bootstrap first :

        cd theme
        git clone https://github.com/bmbrands/theme_bootstrap.git
        mv theme_bootstrap bootstrap

### Grunt CLI via npm (as root)

        apt-get install -y nodejs nodejs-legacy npm
        npm install -g grunt-cli

## Installation

### Compile LESS files

        npm install
        grunt compile

## Dev environment

### Watch for modification in less files

        grunt
